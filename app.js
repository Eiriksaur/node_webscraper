const puppeteer = require('puppeteer');
const fs = require('fs');

(async () => {
    try{
        //Launches the browser.
        const browser = await puppeteer.launch();
        //Initiates a new page.
        const page = await browser.newPage();
        //Link to the website we wish to scrape.
        const link = 'https://no.wikipedia.org/wiki/S%C3%B8kerobot';
        //Connects to the page and waits for it to load propperly.
        await page.goto(link, {waitUntil: 'networkidle2'});
        //The evaluate function that allows us to scrape the data.
        let scrape = await page.evaluate(() => {
            let headlines = [];
            let references = [];
            let topic = document.querySelector('h1[id="firstHeading"]').innerText;
            let headlineSpans = document.querySelectorAll('span.mw-headline');
            let referenceSpans = document.querySelectorAll('span.reference-text');
            for(let i = 0; i< headlineSpans.length; i++){
                headlines.push(headlineSpans[i].innerText);
            }
            for(let j = 0; j< referenceSpans.length; j++){
                references.push(referenceSpans[j].innerText);
            }
            return {
                topic,
                headlines,
                references
            };
        })
        jsonScrape = JSON.stringify(scrape);
        fs.writeFileSync('scrapeData.json', jsonScrape);
        await browser.close();
    } catch(e) {
        console.log(e.message);
    }
})();